<form action="{{ route('index.car') }}" method="get">
    <div>
        <p class="mt-3 mb-1">Брэнд машины машины:</p>
        @foreach($brands as $brand)
            <input class="ms-3 my-2" type="checkbox" name="brand[]" value="{{$brand->id}}"> {{$brand->name}}
        @endforeach
    </div>
    <div>
        <p class="mt-3 mb-1">Модель машины:</p>
        @foreach($carmodels as $carmodel)
            <input class="ms-3 my-2" type="checkbox" name="carmodel[]" value="{{$carmodel->id}}"> {{ $carmodel->name }}

        @endforeach
    </div>
    <div>
        <p class="mt-3 mb-1">Тип двигателя:</p>
        @foreach($engines as $engine)
            <input class="ms-3 my-2" type="checkbox" name="engine[]" value="{{$engine->id}}"> {{$engine->name}}

        @endforeach
    </div>
    <div>
        <p class="mt-3 mb-1">Тип привода:</p>
        @foreach($privods as $privod)
            <input class="ms-3 my-2" type="checkbox" name="privod[]" value="{{$privod->id}}"> {{ $privod->name }}

        @endforeach
    </div>

    <button class="btn btn-outline-secondary mt-3" type="submit">
        <a href="{{ route('index.car') }}">Сбросить</a>
    </button>

    <button class="btn btn-outline-secondary mt-3" type="submit">Поиск</button>
</form>
