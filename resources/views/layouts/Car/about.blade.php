@extends('layouts.main')

@section('content')
    <div class="d-flex flex-column align-items-center">
        <h2 class="mt-5">About us</h2>

        <div class="mt-5 d-flex justify-content-between w-100 p-2 py-5 bg-dark">
            <img src="/storage/app/public/images/SJAAAgIgTeA-1920.jpg" alt="img" class="img-thumbnail">

            <div class="card text-white mb-3 bg-dark" style="max-width: 35rem;">
                <div class="card-body">
                    <h5 class="card-title">Dark card title</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci
                        aliquid
                        animi atque deserunt esse fugiat illum iure minus, officiis, soluta, vero. Accusantium iure
                        maxime quod veritatis. Culpa, suscipit, voluptatibus?</p>
                </div>
            </div>
        </div>


        <div class="mt-5 d-flex justify-content-around w-100 p-2 py-5 bg-dark">
            <div class="card text-white bg-dark mb-3" style="max-width: 35rem;">
                <div class="card-body">
                    <h5 class="card-title">Dark card title</h5>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci
                        aliquid
                        animi atque deserunt esse fugiat illum iure minus, officiis, soluta, vero. Accusantium iure
                        maxime quod veritatis. Culpa, suscipit, voluptatibus?</p>
                </div>
            </div>

            <img src="/storage/app/public/images/SJAAAgIgTeA-1920.jpg" alt="img" class="img-thumbnail">

        </div>

    </div>
@endsection
