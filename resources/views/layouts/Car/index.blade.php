@extends('layouts.main')
@section('content')

    <div class="mt-5">
        <h3>filter</h3>
        <div class="mt-3">
            @include('layouts.Car.components.filter')
        </div>
    </div>
    <div class="mt-5 row">
        <h2>
            {{ $text }}
        </h2>
        @foreach($cars as $car)
            <div class="col-sm-4 mt-5">
                <div class="card" style="width: 20rem;">
                    <img class="card-img-top" src="{{ $car->img }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{ $car->model }}</h5>
                        <p>Brand: {{ $car->brand['name'] }}</p>
                        <p>Type engine: {{ $car->engine }}</p>
                        <p>Type privod: {{ $car->privod }}</p>

                        <p class="card-text">{{ $car->description }}</p>
                        <a href="{{ route('show.car', $car->id) }}" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="mt-5 d-flex justify-content-center">{{ $cars->withQueryString()->links() }}</div>
@endsection
