<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
            crossorigin="anonymous"></script>
</head>
<body>
<header class="p-3 bg-dark text-white">
    <div class="container">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li><a href="{{ route('index.car') }}" class="nav-link px-2 text-secondary">
                        <ya-tr-span data-index="18-0" data-translated="true" data-source-lang="en"
                                    data-target-lang="ru" data-value="Home" data-translation="Главная" data-ch="1"
                                    data-type="trSpan">Главная
                        </ya-tr-span>
                    </a>
                <li><a href="" class="nav-link px-2 text-white">
                        <ya-tr-span data-index="21-0" data-translated="true" data-source-lang="en"
                                    data-target-lang="ru" data-value="FAQs" data-translation="Вопросы и ответы"
                                    data-ch="1" data-type="trSpan">Вопросы и ответы
                        </ya-tr-span>
                    </a></li>
                <li><a href="#" class="nav-link px-2 text-white">
                        <ya-tr-span data-index="22-0" data-translated="true" data-source-lang="en"
                                    data-target-lang="ru" data-value="About" data-translation="О нас" data-ch="1"
                                    data-type="trSpan">О нас
                        </ya-tr-span>
                    </a></li>
            </ul>

            <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
                <input type="search" class="form-control form-control-dark" placeholder="Поиск..."
                       aria-label="Поиск">
            </form>

            <div class="text-end">
                <button type="button" class="btn btn-outline-light me-2">
                    <ya-tr-span data-index="23-0" data-translated="true" data-source-lang="en" data-target-lang="ru"
                                data-value="Login" data-translation="Вход" data-ch="1" data-type="trSpan">Вход
                    </ya-tr-span>
                </button>
                <button type="button" class="btn btn-warning">
                    <ya-tr-span data-index="23-1" data-translated="true" data-source-lang="en" data-target-lang="ru"
                                data-value="Sign-up" data-translation="Регистрация" data-ch="1" data-type="trSpan">
                        Регистрация
                    </ya-tr-span>
                </button>
            </div>
        </div>
    </div>
</header>

<div class="container">
    @yield('content')
</div>
</body>
</html>
