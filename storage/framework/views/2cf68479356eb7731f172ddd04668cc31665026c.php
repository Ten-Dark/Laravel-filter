<?php $__env->startSection('content'); ?>

    <div class="mt-5">
        <h3>filter</h3>
        <div class="mt-3">
            <?php echo $__env->make('layouts.Car.components.filter', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
    </div>
    <div class="mt-5 row">
        <h2>
            <?php echo e($text); ?>

        </h2>
        <?php $__currentLoopData = $cars; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $car): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-sm-4 mt-5">
                <div class="card" style="width: 20rem;">
                    <img class="card-img-top" src="<?php echo e($car->img); ?>" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo e($car->model); ?></h5>
                        <p>Brand: <?php echo e($car->brand['name']); ?></p>
                        <p>Type engine: <?php echo e($car->engine); ?></p>
                        <p>Type privod: <?php echo e($car->privod); ?></p>

                        <p class="card-text"><?php echo e($car->description); ?></p>
                        <a href="<?php echo e(route('show.car', $car->id)); ?>" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>

    <div class="mt-5 d-flex justify-content-center"><?php echo e($cars->withQueryString()->links()); ?></div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/moder/PhpstormProjects/filter/resources/views/layouts/Car/index.blade.php ENDPATH**/ ?>