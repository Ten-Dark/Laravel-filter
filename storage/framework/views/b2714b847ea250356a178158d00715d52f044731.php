<form action="<?php echo e(route('index.car')); ?>" method="get">
    <div>
        <p class="mt-3 mb-1">Брэнд машины машины:</p>
        <?php $__currentLoopData = $brands; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $brand): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <input class="ms-3 my-2" type="checkbox" name="brand[]" value="<?php echo e($brand->id); ?>"> <?php echo e($brand->name); ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div>
        <p class="mt-3 mb-1">Модель машины:</p>
        <?php $__currentLoopData = $carmodels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carmodel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <input class="ms-3 my-2" type="checkbox" name="carmodel[]" value="<?php echo e($carmodel->id); ?>"> <?php echo e($carmodel->name); ?>


        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div>
        <p class="mt-3 mb-1">Тип двигателя:</p>
        <?php $__currentLoopData = $engines; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $engine): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <input class="ms-3 my-2" type="checkbox" name="engine[]" value="<?php echo e($engine->id); ?>"> <?php echo e($engine->name); ?>


        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <div>
        <p class="mt-3 mb-1">Тип привода:</p>
        <?php $__currentLoopData = $privods; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $privod): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <input class="ms-3 my-2" type="checkbox" name="privod[]" value="<?php echo e($privod->id); ?>"> <?php echo e($privod->name); ?>


        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>

    <button class="btn btn-outline-secondary mt-3" type="submit">
        <a href="<?php echo e(route('index.car')); ?>">Сбросить</a>
    </button>

    <button class="btn btn-outline-secondary mt-3" type="submit">Поиск</button>
</form>
<?php /**PATH /home/moder/PhpstormProjects/filter/resources/views/layouts/Car/components/filter.blade.php ENDPATH**/ ?>