<?php $__env->startSection('content'); ?>
    <div class="row mt-5">
        <div class="position-relative col-md-8" style="height: 500px;">
            <img class="h-100 w-100 shadow-lg bg-white rounded" src="<?php echo e($car->img); ?>">
            <button class="position-absolute btn py-1 px-3 bg-white text-dark" style="right: 15%; bottom: 5%">
                <i class="bi bi-heart me-1"></i>
                favorite
            </button>
            <button class="position-absolute btn py-1 px-4 btn-dark text-white" style="right: 5%; bottom: 5%">
                Buy
            </button>
        </div>
        <div class="py-3 col d-flex flex-column justify-content-around shadow-lg bg-white rounded">
            <div class="mt-4 d-flex justify-content-between align-items-center"><h5>Характеристики</h5></div>

            <hr class="mt-4">

            <div class="d-flex align-items-center">
                <div><b>Price:</b></div>
                <div class="ms-4" style="font-size: 20px"><?php echo e($car->price); ?></div>
            </div>

            <hr class="my-1">

            <div class="d-flex align-items-center">
                <div><b>Model:</b></div>
                <div class="ms-4"><?php echo e($car->model); ?></div>
            </div>

            <hr class="my-1">

            <div class="d-flex align-items-center">
                <div><b>Type engine:</b></div>
                <div class="ms-4"><?php echo e($car->engine); ?></div>
            </div>

            <hr class="my-1">

            <div class="d-flex align-items-center">
                <div><b>Type drive unit:</b></div>
                <div class="ms-4"><?php echo e($car->privod); ?></div>
            </div>
        </div>
    </div>
    <div class="row mt-3 gap-1">
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/moder/PhpstormProjects/filter/resources/views/layouts/Car/show.blade.php ENDPATH**/ ?>