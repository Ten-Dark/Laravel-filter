<?php

namespace Database\Seeders;

use App\Models\Car\Privod;
use Illuminate\Database\Seeder;

class PrivodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $privods = [
            ['name' => 'full'],
            ['name' => 'front'],
        ];
        foreach ($privods as $privod) {
            Privod::create($privod);
        }
    }
}
