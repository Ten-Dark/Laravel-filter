<?php

namespace Database\Seeders;

use App\Models\Car\Engine;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EngineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $engines = [
            ['name' => 'dizel'],
            ['name' => 'benzin'],
            ['name' => 'hybrid'],
        ];
        foreach ($engines as $engine) {
            Engine::create($engine);
        }
    }
}
