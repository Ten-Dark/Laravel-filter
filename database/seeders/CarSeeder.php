<?php

namespace Database\Seeders;

use App\Models\Car\Car;
use Illuminate\Database\Seeder;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars =
            [
                [
                    'name' => 'Tayota Corolla',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Corolla',
                    'engine' => 'dizel',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Prius',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Corolla',
                    'engine' => 'benzin',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Camry',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Camry',
                    'engine' => 'dizel',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Camry гибрид',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Camry',
                    'engine' => 'benzin',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Corolla гибрид',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Corolla',
                    'engine' => 'dizel',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota GR Supra',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Camry',
                    'engine' => 'hybrid',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Corolla Hatchback',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Corolla',
                    'engine' => 'dizel',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Crown',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Corolla',
                    'engine' => 'hybrid',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota GR86',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Camry',
                    'engine' => 'benzin',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota GR Corolla',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Corolla',
                    'engine' => 'hybrid',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Mirai',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Camry',
                    'engine' => 'dizel',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Tayota Prius Prime',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'Corolla',
                    'engine' => 'benzin',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-RX ES',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'ES',
                    'engine' => 'hybrid',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-GX',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'GX',
                    'engine' => 'dizel',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-UX ES',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'ES',
                    'engine' => 'benzin',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-Обновленный ES',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'ES',
                    'engine' => 'dizel',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-LC GX',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'GX',
                    'engine' => 'hybrid',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-LS GX',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'GX',
                    'engine' => 'dizel',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-Гибрид UX ES',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'ES',
                    'engine' => 'benzin',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-RC F GX',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'GX',
                    'engine' => 'dizel',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus-Гибрид NX ES',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'ES',
                    'engine' => 'hybrid',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus - Гибрид CT ES',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'ES',
                    'engine' => 'benzin',
                    'privod' => 'front',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
                [
                    'name' => 'Lexus - GS GX',
                    'img' => fake()->imageUrl(150, 150),
                    'description' => fake()->text(100),
                    'model' => 'GX',
                    'engine' => 'dizel',
                    'privod' => 'full',
                    'price' => fake()->numberBetween(300000, 100000)
                ],
            ];


        shuffle($cars);

        foreach ($cars as $item) {
            if (str_starts_with($item['name'], 'Lexus')) {
                $item['brand_id'] = 1;  // LEXUS
                if ($item['model'] == 'ES') {
                    $item['carmodel_id'] = 1;
                } elseif ($item['model'] == 'GX') {
                    $item['carmodel_id'] = 2;
                }


            } else {
                $item['brand_id'] = 2;  // TAYOTA

                if ($item['model'] == 'Camry') {
                    $item['carmodel_id'] = 3;
                } else {
                    $item['carmodel_id'] = 4;
                }
            }

            switch ($item['engine']) {
                case 'dizel':
                    $item['engine_id'] = 1;
                    break;
                case 'benzin':
                    $item['engine_id'] = 2;
                    break;
                case 'hybrid':
                    $item['engine_id'] = 3;
                    break;
            }

            switch ($item['privod']) {
                case 'full':
                    $item['privod_id'] = 1;
                    break;
                case 'front':
                    $item['privod_id'] = 2;
                    break;
            }
            Car::create($item);
        }

    }
}
