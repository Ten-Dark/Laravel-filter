<?php

namespace Database\Seeders;

use App\Models\Car\Carmodel;
use Illuminate\Database\Seeder;

class CarmodelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = [
            ['name' => 'ES'],
            ['name' => 'GX'],
            ['name' => 'Camry'],
            ['name' => 'Corolla'],
        ];

        foreach ($models as $model) {
            Carmodel::create($model);
        }
    }
}
