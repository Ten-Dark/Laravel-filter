<?php

namespace Database\Seeders;

use App\Models\Car\Brand;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            ['name' => 'lexus'],
            ['name' => 'tayota'],
        ];
        foreach ($brands as $brand) {
            Brand::create($brand);
        }
    }
}
