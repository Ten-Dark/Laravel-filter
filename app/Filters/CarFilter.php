<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class CarFilter extends AbstractFilter
{
    public const NAME = 'name';
    public const DESCRIPTION = 'description';
    public const CARMODEL_ID = 'carmodel_id';
    public const BRAND_ID = 'brand_id';
    public const ENGINE_ID = 'engine_id';
    public const PRIVOD_ID = 'privod_id';

    protected function getCallBacks(): array
    {
        return [
            self::NAME => [$this, 'name'],
            self::DESCRIPTION => [$this, 'description'],
            self::CARMODEL_ID => [$this, 'carmodel_id'],
            self::BRAND_ID => [$this, 'brand_id'],
            self::ENGINE_ID => [$this, 'engine_id'],
            self::PRIVOD_ID => [$this, 'privod_id'],
        ];
    }

    public function brand_id(Builder $builder, $value)
    {
        $builder->where('brand_id', 'like', $value);
    }
    public function carmodel_id(Builder $builder, $value)
    {
        $builder->where('carmodel_id', 'like', $value);
    }
    public function engine_id(Builder $builder, $value)
    {
        $builder->where('engine_id', 'like', $value);
    }
    public function privod_id(Builder $builder, $value)
    {
        $builder->where('privod_id', 'like', $value);
    }


    public function name(Builder $builder, $value)
    {
        $builder->where('name', 'like', "%{$value}%");
    }


    public function description(Builder $builder, $value)
    {
        $builder->where('description', 'like', "%{$value}%");
    }

}
