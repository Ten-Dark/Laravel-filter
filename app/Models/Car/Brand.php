<?php

namespace App\Models\Car;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $guarded = false;

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    public function carmodels()
    {
        return $this->hasMany(Carmodel::class);
    }
}
