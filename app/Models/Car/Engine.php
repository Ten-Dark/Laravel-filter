<?php

namespace App\Models\Car;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Engine extends Model
{
    use HasFactory;

    protected $guarded = false;

    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}
