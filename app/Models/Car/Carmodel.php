<?php

namespace App\Models\Car;

use App\Models\traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carmodel extends Model
{
    use HasFactory;
    use Filterable;

    protected $guarded = false;

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
