<?php

namespace App\Models\Car;

use App\Models\traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    use Filterable;
    protected $guarded = false;

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function carmodel()
    {
        return $this->belongsTo(Carmodel::class);
    }

    public function engine()
    {
        return $this->belongsTo(Engine::class);
    }

    public function privod()
    {
        return $this->belongsTo(Privod::class);
    }

}
