<?php

namespace App\Http\Controllers\Car;

use App\Http\Controllers\Controller;
use App\Http\Requests\Car\FilterRequest;
use App\Models\Car\Brand;
use App\Models\Car\Car;
use App\Models\Car\Carmodel;
use App\Models\Car\Engine;
use App\Models\Car\Privod;

class IndexController extends Controller
{
    public function index(FilterRequest $request)
    {
        $brands = Brand::query()->select(['id', 'name'])->get();
        $carmodels = Carmodel::query()->select(['id', 'name'])->get();
        $engines = Engine::query()->select(['id', 'name'])->get();
        $privods = Privod::query()->select(['id', 'name'])->get();

        if ($request->brand or $request->carmodel or $request->engine or $request->privod) {
            $text = 'По фильтру';
            $cars = Car::query()
                ->when(
                    $request->brand,
                    function (\Illuminate\Database\Eloquent\Builder $query) use ($request) {
                        $query->whereIn('brand_id', $request->brand);
                    }
                )
                ->when(
                    $request->carmodel,
                    function (\Illuminate\Database\Eloquent\Builder $query) use ($request) {
                        $query->whereIn('carmodel_id', $request->carmodel);
                    }
                )
                ->when(
                    $request->engine,
                    function (\Illuminate\Database\Eloquent\Builder $query) use ($request) {
                        $query->whereIn('engine_id', $request->engine);
                    }
                )
                ->when(
                    $request->privod,
                    function (\Illuminate\Database\Eloquent\Builder $query) use ($request) {
                        $query->whereIn('privod_id', $request->privod);
                    }
                )->paginate(6);
        } else {
            $text = 'Рекомендуемое';
            $cars = Car::paginate(6);
        }

        return view('layouts.Car.index', compact('cars', 'brands', 'carmodels', 'engines', 'privods', 'text'));


    }
}
