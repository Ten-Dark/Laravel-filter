<?php

namespace App\Http\Controllers\Car;

use App\Http\Controllers\Controller;
use App\Models\Car\Car;

class ShowController extends Controller
{
    public function show(Car $car)
    {
        return view('layouts.Car.show', compact('car'));
    }
}
