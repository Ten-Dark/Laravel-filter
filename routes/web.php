<?php

use App\Http\Controllers\Car\CreateController;
use App\Http\Controllers\Car\DestroyController;
use App\Http\Controllers\Car\EditController;
use App\Http\Controllers\Car\IndexController;
use App\Http\Controllers\Car\ShowController;
use App\Http\Controllers\Car\StoreController;
use App\Http\Controllers\Car\UpdateController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/cars');
});

Route::group(['namespace' => 'Car', 'prefix' => 'cars'], function () {
    Route::get('', [IndexController::class, 'index'])->name('index.car');
    Route::get('create', [CreateController::class, 'create'])->name('create.car');
    Route::post('', [StoreController::class, 'store'])->name('store.car');
    Route::get('{car}', [ShowController::class, 'show'])->name('show.car');
    Route::get('edit', [EditController::class, 'edit'])->name('edit.car');
    Route::get('update', [UpdateController::class, 'update'])->name('update.car');
    Route::get('update', [DestroyController::class, 'destroy'])->name('destroy.car');
});



